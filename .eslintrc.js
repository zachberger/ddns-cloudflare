module.exports = {
    "extends": "airbnb-base",
    "env": {
        "node": true
    },
    "rules": {
        "no-console": 0,
        "max-len": 0
    }
};