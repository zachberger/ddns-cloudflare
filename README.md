# Cloudflare DDNS

This module updates a CloudFlare host's AAAA records with the current machine's IPv6 addresses

## Configuration


###  Configuration File
Create a `.json` file in the following format

```json
{
	"interface": "", 
	"email": "", 
	"key": "", 
	"zone": "", 
	"name": ""
}
```

### Configuring execution on address change

#### Leveraging systemd instantiated services


1. Create a new service file at `/etc/systemd/system/ddns-cloudflare@.service` with the following contents:

	```systemd
	[Unit]
	Description=Update CloudFlare with latest IP addresses from %I
	BindsTo=sys-subsystem-net-devices-%i.device
	After=sys-subsystem-net-devices-%i.device
	Wants=network-online.target
	After=network-online.target

	[Service]
	Type=oneshot
	RemainAfterExit=yes
	ExecStart=/usr/local/bin/ddns-cloudflare -c /path/to/config.json -i %I

	[Install]
	WantedBy=multi-user.target
	```

1. Start the servie `sudo systemctl start ddns-cloudflare@enp2s0`
1. Enable the service `sudo systemctl enable ddns-cloudflare@enp2s0`

Repeat steps 2 and 3 for each additional network interface replacing `enp2s0` with the desired interfaces.

Additional information is availble on this [blog post about instantiated services](http://0pointer.de/blog/projects/instances.html) and this StackExchange [question on detecting IP address changes](https://unix.stackexchange.com/questions/159641/systemd-restart-service-when-ip-address-changed).


## Usage

To test or to update just once simply call ddns-cloudflare providing the config file 

```bash
ddns-cloudflare -c /path/to/config.json
```