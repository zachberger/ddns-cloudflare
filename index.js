#!/usr/bin/env node

const os = require('os');
const fs = require('fs');
const Cf = require('cloudflare4');
const request = require('request-promise');
const argv = require('yargs')
  .usage('Usage: $0 -c [configuration file]')
  .alias('c', 'config')
  .demandOption(['c'])
  .describe('c', 'path to configuraiton file')
  .alias('i', 'interface')
  .describe('i', 'Override the interface declared in the configuration file')
  .boolean('s')
  .default('s', false)
  .describe('s', 'skip updating of host')
  .help('h')
  .alias('h', 'help')
  .argv;

const configFileContents = fs.readFileSync(argv.c);
const config = JSON.parse(configFileContents);

if (argv.i) {
  config.interface = argv.i;
}

const cf = new Cf({
  email: config.email,
  key: config.key,
});

const logAddresses = (addresses, label) => {
  if (label) {
    console.log(`${label}: `);
  }
  console.log(`\t${addresses.sort().join(',\n\t')}`);
};

const updateAddressesByFamily = async (family) => {
  try {
    const remoteAddressPromise = cf.zoneDNSRecordGetAll(config.zone, {
      type: family.type,
      name: config.name,
    });
    const localAddressPromise = family.locator();
    const [remoteAddresses, localAddresses] = await Promise.all([remoteAddressPromise, localAddressPromise]);
    logAddresses(remoteAddresses.map(a => a.content), `Remote ${family.name} Addresses`);
    logAddresses(localAddresses, `Local ${family.name} Addresses`);

    const staleAddresses = remoteAddresses.filter(address => !localAddresses.includes(address.content));
    logAddresses(staleAddresses.map(a => a.content), `Stale ${family.name} Addresses`);

    const newAddresses = localAddresses.filter(address => !remoteAddresses.map(a => a.content).includes(address));
    logAddresses(newAddresses, `New ${family.name} Addresses`);

    const removalPromises = argv.s ? [] : staleAddresses.map(address => cf.zoneDNSRecordDestroy(config.zone, address.id));
    const additionPromises = argv.s ? [] : newAddresses.map(address => cf.zoneDNSRecordNew(config.zone, {
      type: family.type,
      name: config.name,
      content: address,
      ttl: 1,
    }));

    await Promise.all([...removalPromises, ...additionPromises]);
    console.log(`Successfully updated ${family.name} addresses`);
  } catch (error) {
    console.error(`Unable to update ${family.name} addresses: ${error}`);
  }
};

const IPv4 = {
  name: 'IPv4',
  type: 'A',
  locator: () => {
    const options = {
      uri: 'https://ipinfo.io/json',
      json: true,
    };
    return request.get(options).then(response => [response.ip]);
  },
};

const IPv6 = {
  name: 'IPv6',
  type: 'AAAA',
  locator: () => {
    const networkInterface = os.networkInterfaces()[config.interface];
    if (!networkInterface) return Promise.reject(`Unknown interface ${config.interface}`);
    const addresses = networkInterface.filter(x => x.family === IPv6.name && x.scopeid === 0).map(x => x.address);
    return Promise.resolve(addresses);
  },
};

updateAddressesByFamily(IPv6);
updateAddressesByFamily(IPv4);
